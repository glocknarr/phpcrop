<?php
/**
 * phpCrop
 * Обрезание изображений до квадрата
 * @param array $photos - массив наименований файлов
 * @param string $directory - директория для сохранения обрезанных файлов. По умолчанию /images
 * @return array $success
 */
function phpCrop(array $photos, $directory = '/images')
{
    $root = $_SERVER['DOCUMENT_ROOT'];
    //=======================================================================
    //todo: пути указаны для тестирования на моем компе.
    //    $defaultDir = $root . '/my/phpcrop'.$directory;//директория откуда брать файлы
    //    $destDir = $root . '/my/phpcrop'.$directory;
    //=======================================================================
    $defaultDir = $root .$directory;//директория откуда брать файлы
    $destDir = $root .$directory; //куда сохранять файлы

    $success = [];
    $validType = array(
        0 => 'UNKNOWN',
        1 => 'GIF',
        2 => 'JPEG',
        3 => 'PNG',
        4 => 'SWF',
        5 => 'PSD',
        6 => 'BMP',
        7 => 'TIFF_II',
        8 => 'TIFF_MM',
        9 => 'JPC',
        10 => 'JP2',
        11 => 'JPX',
        12 => 'JB2',
        13 => 'SWC',
        14 => 'IFF',
        15 => 'WBMP',
        16 => 'XBM',
        17 => 'ICO',
        18 => 'COUNT'
    );

    foreach ($photos as $photo) {
        $file = $defaultDir . '/' . $photo;
        $minSize = 0;
        $cropResult = '';
        if (file_exists($file)) {
            list($imageWidth, $imageHeight, $imageExtensionId) = getimagesize($file);
            if (!array_key_exists($imageExtensionId, $validType)) {
                $success[$photo] = "Not valid extentsion";
                continue;
            }
            $imageExt = $validType[$imageExtensionId];
            $func = 'imagecreatefrom' . strtolower($imageExt);
            $saveImg = 'image' . strtolower($imageExt);
            $image = $func($file);
            $destFile = $destDir.'/'.$photo;
            if ($imageHeight != $imageWidth) {
                if ($imageWidth < $imageHeight) {
                    $minSize = $imageWidth;
                    $x = 0;
                    $y = floor(($imageHeight - $imageWidth) / 2);
                } else {
                    $minSize = $imageHeight;
                    $x = floor(($imageWidth - $imageHeight) / 2);
                    $y = 0;
                }
                $newImage = imagecrop($image, ['x' => $x, 'y' => $y, 'width' => $minSize, 'height' => $minSize]);
                if ($newImage !== FALSE){
                    $saveImg($newImage, $destFile);
                    $cropResult = "TRUE";
                    imagedestroy($newImage);
                } else {
                    $cropResult = 'FALSE';
                }
                imagedestroy($image);
            } else {
                //изображение уже квадратное
                $cropResult = "TRUE";
                $saveImg($image, $destFile);
                imagedestroy($image);
            }

        } else {
            $cropResult = "No file"; //файла с таким именем нет
        }
        $success[$photo] = $cropResult;
    }
    return $success;
}

//проверка
$examples = ['photo1.jpg', 'photo2.jpg', 'photo3.jpg'];
$ret = phpCrop($examples);
echo '<pre>'; print_r($ret); echo '</pre>';